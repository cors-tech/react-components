const tailWindCss = require("tailwindcss");

module.exports = {
  plugins: [require("postcss-import"), tailWindCss("./src/tailwind.js"), require("autoprefixer")]
};
