Card component
```jsx
<Card 
    mini={true}
    title="Small Card"
    body="Some body content for the card."
    RouterLink={{}}
/>

<div className="mt-4">
   <Card 
        mini={true}
        title="Small Card with image"
        body="Some body content for the card."
        image_src="https://materials.dominochinese.com/misc/anonymous-user.png"
        RouterLink={{}}
   /> 
</div>

<div className="mt-4">
   <Card 
       title="Large card with external link"
       title_url="https://dominochinese.com"
       body="Some body content for the card."
       RouterLink={{}}
   /> 
</div>

<div className="mt-4">
   <Card 
       title="Card with local React Link"
       title_url="http://example.org"
       body="Some body content for the card."
       RouterLink={{}}
   /> 
</div>

<div className="mt-4">
   <Card 
       title="Large card with image"
       body="Some body content for the card."
       image_src="https://materials.dominochinese.com/misc/anonymous-user.png"
       RouterLink={{}}
   /> 
</div>
```