import React from "react";
import PropTypes from "prop-types";
import Panel from "./Panel";
import Input from "./Input";
import Button from "./Button";
import debounce from "lodash/debounce";
import * as QS from "query-string";
import { parseErrorMessage } from "@iforwms/helpers-js";

class SignUpForm extends React.Component {
  state = {
    isSubmitting: false,
    isVerifyingCoupon: false,
    referrer: "",
    email: "",
    password: "",
    password_confirmation: "",
    coupon: "",
    error: null
  };

  verifyCoupon = debounce(async () => {
    const { post, toast } = this.props;

    this.safeSetState({ isVerifyingCoupon: true });

    const { coupon, referrer } = this.state;

    post("verify-coupon", { coupon, referrer })
      .then(() => {
        this.safeSetState({ isVerifyingCoupon: false });

        if (toast) {
          toast.success("Coupon verified!");
        }
      })
      .catch(error => {
        this.safeSetState({ error, isVerifyingCoupon: false });

        if (toast) {
          toast.error(parseErrorMessage(error, "Failed to verify coupon."));
        }
      });
  }, 800);

  handleSubmit = e => {
    e.preventDefault();

    const {
      data: { endpoint },
      post,
      toast,
      onError,
      history,
      onSuccess
    } = this.props;

    const { email, password, password_confirmation, coupon } = this.state;

    this.safeSetState({ isSubmitting: true, error: null });

    post(endpoint, { email, password, password_confirmation, coupon })
      .then(({ data }) => {
        this.safeSetState({ isSubmitting: false });

        if (onSuccess) {
          onSuccess(data);
        }
        if (toast) {
          toast.success("Registration success! Please login.");
        }

        history.push("/login");
      })
      .catch(error => {
        this.safeSetState({ error, isSubmitting: false });

        if (onError) {
          onError(error);
        }

        if (toast) {
          toast.error(
            parseErrorMessage(
              error,
              "Registration failed, please check your credentials."
            )
          );
        }
      });
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.coupon !== "" && this.state.coupon !== prevState.coupon) {
      this.verifyCoupon();
    }
  }

  componentDidMount() {
    this._isMounted = true;

    const query = QS.parseUrl(window.location.search).query;

    if (query && query.coupon) {
      this.safeSetState({ coupon: query.coupon, referrer: document.referrer });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  safeSetState = (...args) => {
    this._isMounted && this.setState(...args);
  };

  setParentState = (valueKey, value) => {
    this.setState({ [valueKey]: value });
  };

  render() {
    const {
      data: {
        hasCoupon,
        containerClasses,
        submitText,
        colour,
        title,
        icon,
        id
      },
      post
    } = this.props;

    const {
      error,
      isSubmitting,
      email,
      coupon,
      password,
      password_confirmation
    } = this.state;

    const formBody = (
      <div>
        <div>
          <Input
            label={`Email`}
            value={email}
            error={error}
            setState={this.setParentState}
            type="email"
            disabled={isSubmitting}
            valueKey={`email`}
          />
        </div>

        <div className="mt-4">
          <Input
            disabled={isSubmitting}
            error={error}
            type={`password`}
            valueKey={"password"}
            label={`Password`}
            value={password}
            setState={this.setParentState}
          />
        </div>

        <div className="mt-4">
          <Input
            disabled={isSubmitting}
            error={error}
            type={`password`}
            valueKey={"password_confirmation"}
            label={`Password Confirmation`}
            value={password_confirmation}
            setState={this.setParentState}
          />
        </div>

        {hasCoupon ? (
          <div className="mt-4">
            <Input
              disabled={isSubmitting}
              error={error}
              valueKey={`coupon`}
              label={`Coupon`}
              value={coupon}
              setState={this.setParentState}
            />
          </div>
        ) : null}

        <div className="mt-4">
          <Button colour={colour} disabled={isSubmitting} text={submitText} />
        </div>
      </div>
    );

    return (
      <div id={id} className={containerClasses}>
        <Panel colour={colour} title={title} icon={icon}>
          {post ? (
            <form method="POST" onSubmit={this.handleSubmit}>
              {formBody}
            </form>
          ) : (
            formBody
          )}
        </Panel>
      </div>
    );
  }
}

SignUpForm.propTypes = {
  data: PropTypes.shape({
    endpoint: PropTypes.string.isRequired,
    hasSubscribe: PropTypes.bool,
    hasCoupon: PropTypes.bool,
    containerClasses: PropTypes.string,
    submitText: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    icon: PropTypes.string,
    title: PropTypes.string
  }),
  post: PropTypes.func,
  isEditing: PropTypes.bool.isRequired
};

SignUpForm.defaultProps = {
  isEditing: false
};

export default SignUpForm;
