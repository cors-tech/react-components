A panel component.
```jsx
<Panel title={`Label text`}/>

<div className="mt-4">
<Panel title={`Panel with coloured background`} background={`red`} colour={`red`}/>
</div>

<div className="mt-4">
<Panel title={`Panel with child elements`} colour={`pink`}>
  <h1>Child element</h1>
  <p>Another child element</p>
</Panel>
</div>

<div className="mt-4">
<Panel icon={`alert`} title={`Panel Heading`} colour={`blue`}>
  <h1>Panel with an icon</h1>
</Panel>
</div>
```