Generic button component.
```jsx
<Button text={`Button Text`}/>

<div className={`mt-4`}>
<Button colour={`green`} text={`Green Button (Not full-width)`} fullWidth={false}/>
</div>

<div className={`mt-4`}>
<Button colour={`pink`} size={`xs`} text={`Small Button`}/>
</div>

<div className={`mt-4`}>
<Button colour={`red`} text={`Button with Callback`} onClick={() => alert('Hey!')}/>
</div>

<div className={`mt-4`}>
<Button colour={`indigo`} text={`Button with Icon`} icon={`lock`}/>
</div>
 
```