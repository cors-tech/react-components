A generic form input.        
```jsx
<Input valueKey="abc" />

<div className="mt-4">
  <Input valueKey="abc" label={`Input with label`}/>
</div>

<div className="mt-4">
  <Input valueKey="abc" type={`checkbox`} label={`A checkbox label`}/>
</div>

<div className="mt-4">
  <Input valueKey="abc" type={`textarea`} label={`A disabled textarea`} disabled={true}/>
</div>

<div className="mt-4">
  <Input valueKey="abc" type={`password`} label={`Password Input`}/>
</div>
```