import React from "react";
import PropTypes from "prop-types";
import Button from "./forms/Button";

const Card = ({
  title,
  title_url,
  body,
  image_src,
  mini,
  children,
  isChild,
  classes,
  header,
  RouterLink
}) => {
  if (mini) {
    return (
      <div
        className={`flex items-center ${
          isChild ? "" : "rounded shadow bg-white w-full p-4"
        } ${classes}`}>
        <div className={`flex`}>
          {header ? header : null}

          {image_src ? (
            <div
              className={`bg-cover bg-center mr-4 rounded`}
              style={{
                height: 50,
                minWidth: 50,
                width: 50,
                backgroundImage: `url(${image_src})`
              }}
            />
          ) : null}

          <div className={`flex flex-col justify-start leading-tight`}>
            {title ? (
              <div className={`uppercase text-xs font-bold tracking-wide`}>{title}</div>
            ) : null}

            {body ? <div className={`mt-1`}>{body}</div> : null}
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className={`flex flex-col bg-white shadow rounded overflow-hidden ${classes}`}>
      {header ? header : null}

      {image_src ? (
        <div className={`flex-shrink-0`}>
          <img className={`h-48 object-cover w-full`} src={image_src} alt="" />
        </div>
      ) : null}

      <div className={`flex flex-col flex-1 justify-between p-4`}>
        <div className={`text-sm`}>
          <h2
            className={`mt-2 text-xl leading-tight font-bold ${
              title_url ? "hover:underline" : ""
            }`}>
            {title_url ? (
              <Button isLink={true} RouterLink={RouterLink} url={title_url} text={title} />
            ) : (
              title
            )}
          </h2>

          <div className={`mt-2 text-sm font-light text-gray-600`}>{body}</div>
        </div>

        {children ? <div>{children}</div> : null}
      </div>
    </div>
  );
};

Card.propTypes = {
  title_url: PropTypes.string,
  title: PropTypes.string,
  classes: PropTypes.string,
  body: PropTypes.string,
  RouterLink: PropTypes.any,
  header: PropTypes.any,
  isChild: PropTypes.bool.isRequired,
  children: PropTypes.any,
  mini: PropTypes.bool.isRequired,
  image_src: PropTypes.string
};

Card.defaultProps = {
  isChild: false,
  mini: false
};

export default Card;
