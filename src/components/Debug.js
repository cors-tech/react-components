import React from "react";
import Panel from "./forms/Panel";

const Debug = ({ children }) =>
  children && process.env.NODE_ENV === "development" ? (
    <div className={`max-w-xl mx-auto m-4`}>
      <Panel title={`Debug Info`} colour={`red`} icon={`bug`}>
        <pre className={`mt-2`}>{JSON.stringify(children, null, 4)}</pre>
      </Panel>
    </div>
  ) : null;

export default Debug;
