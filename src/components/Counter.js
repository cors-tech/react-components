import React from "react";
import PropTypes from "prop-types";

const Counter = ({ start, end, duration, decimals }) => <div>COUNTER!</div>;

Counter.propTypes = {
  start: PropTypes.number.isRequired,
  end: PropTypes.number.isRequired,
  duration: PropTypes.number.isRequired,
  decimals: PropTypes.number.isRequired
};
