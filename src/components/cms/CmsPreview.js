import React, { Component } from "react";
import PropTypes from "prop-types";
import CmsSocialLinks from "./CmsSocialLinks";
import CmsImage from "./CmsImage";
import CmsPanel from "./CmsPanel";
import CmsCard from "./CmsCard";
import CmsVideo from "./CmsVideo";
import CmsButton from "./CmsButton";
import CmsMarkdown from "./CmsMarkdown";
import CmsHtml from "./CmsHtml";
import LoadingWrapper from "../LoadingWrapper";
import PageHeader from "../PageHeader";
import Button from "../forms/Button";
import SignUpForm from "../forms/SignUpForm";

const components = {
  social_links: CmsSocialLinks,
  image: CmsImage,
  panel: CmsPanel,
  card: CmsCard,
  video: CmsVideo,
  button: CmsButton,
  sign_up_form: SignUpForm,
  markdown: CmsMarkdown,
  html: CmsHtml,
  text: CmsHtml
};

class CmsPreview extends Component {
  state = {
    title: "",
    subtitle: "",
    author: "",
    id: "",
    slug: "",
    body: [],
    published_at: "",
    login_required: false,
    newSection: "",
    isLoading: false,
    error: null
  };

  componentDidMount() {
    this._isMounted = true;
    this.fetchItem();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  safeSetState = (...args) => {
    this._isMounted && this.setState(...args);
  };

  componentDidUpdate(prevProps) {
    if (
      (prevProps.match &&
        prevProps.match.params.slug !== this.props.match.params.slug) ||
      prevProps.slug !== this.props.slug
    ) {
      this.fetchItem();
    }
  }

  fetchItem = () => {
    this.safeSetState({ isLoading: true, error: null });

    const slug = this.props.slug
      ? this.props.slug
      : this.props.match.params.slug;

    const url = this.props.isBlogPost ? `posts/${slug}` : `pages/${slug}`;

    this.props
      .get(url)
      .then(({ data: { data } }) => {
        this.safeSetState({
          title: data.title,
          author: data.author,
          subtitle: data.subtitle,
          bg_image: data.bg_image,
          slug: data.slug,
          id: data.id,
          body: data.body,
          published_at: data.published_at,
          isLoading: false
        });
      })
      .catch(error => {
        this.safeSetState({ isLoading: false, error });
        // history.push("/404");
      });
  };

  render() {
    const {
      id,
      isLoading,
      body,
      title,
      error,
      subtitle,
      author,
      published_at,
      bg_image
    } = this.state;

    const {
      hasHeader,
      isBlogPost,
      Comments,
      RouterLink,
      isAdmin,
      toast,
      history,
      post
    } = this.props;

    const slug = this.props.slug
      ? this.props.slug
      : this.props.match.params.slug;

    return (
      <div className="static-content">
        <div>
          <LoadingWrapper
            isLoading={isLoading}
            error={error}
            retry={this.fetchItem}>
            {isAdmin ? (
              <Button
                RouterLink={RouterLink}
                fullWidth={false}
                size={`sm`}
                colour={`orange`}
                classes="z-10 fixed top-0 right-0 m-2"
                url={isBlogPost ? `/posts/${slug}` : `/pages/${slug}`}>
                Edit
              </Button>
            ) : null}

            {hasHeader && (
              <PageHeader
                title={title}
                description={subtitle}
                author={author}
                published_at={published_at}
              />
            )}

            <div className={`body`}>
              {body.map((section, index) => {
                const Section = components[section.type];
                return (
                  <div key={index} className="">
                    <Section
                      post={post}
                      title={title}
                      toast={toast}
                      history={history}
                      image={bg_image}
                      data={section.data}
                      RouterLink={RouterLink}
                    />
                  </div>
                );
              })}
            </div>

            {isBlogPost && id && Comments ? (
              <Comments type={`post`} id={id} />
            ) : null}
          </LoadingWrapper>
        </div>
      </div>
    );
  }
}

CmsPreview.propTypes = {
  get: PropTypes.func.isRequired,
  post: PropTypes.func,
  Comments: PropTypes.func,
  RouterLink: PropTypes.object.isRequired,
  hasHeader: PropTypes.bool.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  slug: PropTypes.string,
  isBlogPost: PropTypes.bool,
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string
    })
  })
};

CmsPreview.defaultProps = {
  hasHeader: true,
  isAdmin: false,
  isBlogPost: false
};

export default CmsPreview;
