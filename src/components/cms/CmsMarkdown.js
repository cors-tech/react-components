import React, { Component } from "react";
import PropTypes from "prop-types";
import Markdown from "react-markdown";
import Input from "../forms/Input";

class CmsMarkdown extends Component {
  state = {};

  handleSubmit = value => {
    // console.log("markdown update", this.state.data);
    this.props.onUpdate(value);
  };

  setParentState = (valueKey, value) => {
    this.setState({ [valueKey]: value }, () => this.handleSubmit(value));
  };

  render() {
    const { data, isEditing } = this.props;

    if (isEditing) {
      return (
        <div>
          <div>
            <a
              className="text-xs text-pink-500 underline hover:text-pink-700"
              href="https://markdownguide.org/basic-syntax"
              target="_blank"
              rel="noopener noreferrer">
              Markdown Guide
            </a>
          </div>

          <Input
            classes="mt-1"
            type={`textarea`}
            valueKey={`data`}
            setState={this.setParentState}
            rows={8}
            value={data}
          />
        </div>
      );
    }

    return <Markdown source={data} />;
  }
}

CmsMarkdown.propTypes = {
  data: PropTypes.string.isRequired,
  isEditing: PropTypes.bool.isRequired,
  onUpdate: PropTypes.func
};

CmsMarkdown.defaultProps = {
  isEditing: false
};

export default CmsMarkdown;
