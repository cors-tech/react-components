import React from "react";
import PropTypes from "prop-types";
import Label from "../forms/Label";
import Input from "../forms/Input";
import Button from "../forms/Button";

const CmsDate = ({
  type,
  setParentState,
  year,
  month,
  day,
  hour,
  minute,
  label,
  showNow,
  keyPrefix
}) => (
  <div className="">
    <Label label={label} />

    <div className="flex">
      <Input
        setState={setParentState}
        valueKey={`${keyPrefix ? keyPrefix + "_" : ""}year`}
        value={year}
        type={`number`}
        min={1900}
        step={1}
      />

      <Input
        setState={setParentState}
        valueKey={`${keyPrefix ? keyPrefix + "_" : ""}month`}
        value={month}
        type={`number`}
        min={1}
        max={12}
        step={1}
      />

      <Input
        setState={setParentState}
        valueKey={`${keyPrefix ? keyPrefix + "_" : ""}day`}
        value={day}
        type={`number`}
        min={1}
        max={31}
        step={1}
      />

      {type === "datetime" ? (
        <>
          <Input
            setState={setParentState}
            valueKey={`${keyPrefix ? keyPrefix + "_" : ""}hour`}
            value={hour}
            type={`number`}
            min={0}
            max={24}
            step={1}
          />

          <Input
            setState={setParentState}
            valueKey={`${keyPrefix ? keyPrefix + "_" : ""}minute`}
            value={minute}
            type={`number`}
            min={0}
            max={59}
            step={1}
          />
        </>
      ) : null}

      {showNow ? (
        <Button
          type={`button`}
          colour={`pink`}
          classes={`ml-2 whitespace-no-wrap`}
          fullWidth={false}
          onClick={() => {
            setParentState(
              `${keyPrefix ? keyPrefix + "_" : ""}year`,
              new Date().getUTCFullYear().toString()
            );
            setParentState(
              `${keyPrefix ? keyPrefix + "_" : ""}month`,
              (new Date().getUTCMonth() + 1).toString()
            );
            setParentState(
              `${keyPrefix ? keyPrefix + "_" : ""}day`,
              new Date().getUTCDate().toString()
            );
            setParentState(
              `${keyPrefix ? keyPrefix + "_" : ""}hour`,
              new Date().getUTCHours().toString()
            );
            setParentState(
              `${keyPrefix ? keyPrefix + "_" : ""}minute`,
              new Date().getUTCMinutes().toString()
            );
          }}
          text={`Now (UTC)`}
        />
      ) : null}
    </div>
  </div>
);

CmsDate.propTypes = {
  type: PropTypes.string.isRequired,
  setParentState: PropTypes.func.isRequired,
  year: PropTypes.string.isRequired,
  month: PropTypes.string.isRequired,
  day: PropTypes.string.isRequired,
  hour: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  minute: PropTypes.string.isRequired,
  keyPrefix: PropTypes.string.isRequired,
  showNow: PropTypes.bool.isRequired
};

CmsDate.defaultProps = {
  showNow: true,
  label: "(yyyy/mm/dd hh:mm) (UTC)",
  keyPrefix: "",
  type: "datetime"
};

export default CmsDate;
