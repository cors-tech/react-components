import React, { Component } from "react";
import PropTypes from "prop-types";
import Input from "../forms/Input";
import Card from "../Card";
import Label from "../forms/Label";
import MediaUpload from "../forms/MediaUpload";
import CmsMarkdown from "./CmsMarkdown";

class CmsCard extends Component {
  state = {};

  handleSuccess = (file, data) => {
    const payload = { ...this.state, ...data.data };
    this.props.onUpdate(payload);
  };

  handleSubmit = () => {
    // console.log("Panel update", this.state);
    this.props.onUpdate(this.state);
  };

  setParentState = (valueKey, value) => {
    this.setState({ [valueKey]: value }, () => this.handleSubmit());
  };

  render() {
    const {
      data: {
        title,
        title_url,
        classes,
        header,
        isChild,
        mini,
        src,
        containerClasses,
        body,
        content
      },
      post,
      isEditing,
      RouterLink
    } = this.props;

    if (isEditing) {
      return (
        <div>
          <div>
            <Input
              label={`Title`}
              value={title}
              valueKey={`title`}
              setState={this.setParentState}
            />
          </div>

          <div className={`mt-4`}>
            <Input
              label={`Title URL`}
              value={title_url}
              valueKey={`title_url`}
              setState={this.setParentState}
            />
          </div>

          <div className={`mt-4`}>
            <Input
              label={`Classes`}
              value={classes}
              valueKey={`classes`}
              setState={this.setParentState}
            />
          </div>

          <div className="mt-4">
            <Label label={`Image`} />
            <MediaUpload
              accept="image/*"
              post={post}
              onError={() => {}}
              onSuccess={this.handleSuccess}
            />
          </div>

          <div className="mt-4">
            <Input
              label={`Container CSS Classes`}
              value={containerClasses}
              valueKey={`containerClasses`}
              setState={this.setParentState}
            />
          </div>

          <div className="mt-4">
            <Input
              type={`checkbox`}
              setState={this.setParentState}
              label={`Is mini?`}
              value={mini}
              valueKey={`mini`}
            />
          </div>

          <div className="mt-4">
            <Input
              type={`checkbox`}
              setState={this.setParentState}
              label={`Is child?`}
              value={isChild}
              valueKey={`isChild`}
            />
          </div>

          <div className="mt-4">
            <Label label={`Body (Markdown)`} />
            <CmsMarkdown
              onUpdate={data => this.setParentState(`body`, data)}
              isEditing={true}
              label={`Body`}
              value={body}
              valueKey={`body`}
              setState={this.setParentState}
              data={body}
            />
          </div>

          <div className="mt-4">
            <Label label={`Header (Markdown)`} />
            <CmsMarkdown
              onUpdate={data => this.setParentState(`header`, data)}
              isEditing={true}
              label={`Header`}
              value={header}
              valueKey={`header`}
              setState={this.setParentState}
              data={header}
            />
          </div>

          <div className="mt-4">
            <Label label={`Main Content (Markdown)`} />
            <CmsMarkdown
              onUpdate={data => this.setParentState(`content`, data)}
              isEditing={true}
              label={`Main Content`}
              value={content}
              valueKey={`content`}
              setState={this.setParentState}
              data={content}
            />
          </div>
        </div>
      );
    }

    return (
      <div className={containerClasses}>
        <Card
          RouterLink={RouterLink}
          header={<CmsMarkdown data={header} />}
          isChild={isChild}
          mini={mini}
          image_src={src}
          title={title}
          title_url={title_url}
          classes={classes}
          body={<CmsMarkdown data={body} />}>
          <CmsMarkdown data={content} />
        </Card>
      </div>
    );
  }
}

CmsCard.propTypes = {
  data: PropTypes.object.isRequired,
  isEditing: PropTypes.bool.isRequired,
  onUpdate: PropTypes.func
};

CmsCard.defaultProps = {
  isEditing: false
};

export default CmsCard;
