import React from "react";
import PropTypes from "prop-types";
import Input from "../forms/Input";
import SignUpForm from "../forms/SignUpForm";
import SelectInput from "../forms/SelectInput";
import { icons } from "../Icon";

class CmsSignUpForm extends React.Component {
  state = {};

  handleSubmit = () => {
    this.props.onUpdate(this.state);
  };

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  safeSetState = (...args) => {
    this._isMounted && this.setState(...args);
  };

  setParentState = (valueKey, value) => {
    this.setState({ [valueKey]: value }, () => this.handleSubmit());
  };

  render() {
    const {
      data: {
        hasCoupon,
        title,
        colour,
        icon,
        containerClasses,
        submitText,
        endpoint,
        id
      },
      data,
      isEditing,
      isSubmitting
    } = this.props;

    if (isEditing) {
      return (
        <div>
          <div>
            <Input
              disabled={isSubmitting}
              label={`Title`}
              value={title || ""}
              valueKey={`title`}
              setState={this.setParentState}
            />
          </div>
          <div className={`mt-4`}>
            <Input
              disabled={isSubmitting}
              label={`Container Classes`}
              value={containerClasses || ""}
              description="CSS classes for styling the input container."
              valueKey={`containerClasses`}
              setState={this.setParentState}
            />
          </div>

          <div className="mt-4">
            <Input
              fullWidth={false}
              type={`colour`}
              setState={this.setParentState}
              label={`Select a Colour`}
              value={colour}
              valueKey={`colour`}
            />
          </div>

          <div className={`mt-4`}>
            <Input
              disabled={isSubmitting}
              label={`Submit Text`}
              description="The submit button text"
              value={submitText || ""}
              valueKey={`submitText`}
              setState={this.setParentState}
            />
          </div>

          <div className={`mt-4`}>
            <Input
              disabled={isSubmitting}
              label={`endpoint`}
              description="The API endpoint the form should post to."
              value={endpoint || ""}
              valueKey={`endpoint`}
              setState={this.setParentState}
            />
          </div>

          <div className={`mt-4`}>
            <Input
              disabled={isSubmitting}
              label={`id`}
              description="The input ID (not required)"
              value={id || ""}
              valueKey={`id`}
              setState={this.setParentState}
            />
          </div>

          <div className={`mt-4`}>
            <SelectInput
              options={[""].concat(Object.keys(icons))}
              description="Select an icon for the panel or leave blank for no icon."
              disabled={isSubmitting}
              label={`icon`}
              value={icon || ""}
              valueKey={`icon`}
              setState={this.setParentState}
            />
          </div>

          <div className={`mt-4`}>
            <Input
              type="checkbox"
              value={hasCoupon}
              valueKey={`hasCoupon`}
              label="Show Coupon"
              setState={this.setParentState}
            />
          </div>
        </div>
      );
    }

    return <SignUpForm data={data} />;
  }
}

CmsSignUpForm.propTypes = {
  data: PropTypes.shape({
    endpoint: PropTypes.string.isRequired,
    hasSubscribe: PropTypes.bool,
    containerClasses: PropTypes.string,
    submitText: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    icon: PropTypes.string,
    title: PropTypes.string
  }),
  post: PropTypes.func,
  isEditing: PropTypes.bool.isRequired
};

CmsSignUpForm.defaultProps = {
  isEditing: false
};

export default CmsSignUpForm;
