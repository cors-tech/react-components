import React, { Component } from "react";
import PropTypes from "prop-types";
import Input from "../forms/Input";

class CmsHtml extends Component {
  state = {};

  handleSubmit = () => {
    // console.log("html update", this.state.data);
    this.props.onUpdate(this.state.data);
  };

  setParentState = (valueKey, value) => {
    this.setState({ [valueKey]: value }, () => this.handleSubmit());
  };

  render() {
    const { data, isEditing } = this.props;

    if (isEditing) {
      return (
        <Input
          type={`textarea`}
          valueKey={`data`}
          setState={this.setParentState}
          rows={8}
          value={data}
        />
      );
    }

    return <div dangerouslySetInnerHTML={{ __html: data }} />;
  }
}

CmsHtml.propTypes = {
  data: PropTypes.string.isRequired,
  isEditing: PropTypes.bool.isRequired,
  onUpdate: PropTypes.func
};

CmsHtml.defaultProps = {
  isEditing: false
};

export default CmsHtml;
