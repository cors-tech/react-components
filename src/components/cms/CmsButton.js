import React from "react";
import PropTypes from "prop-types";
import Button from "../forms/Button";
import Input from "../forms/Input";
import SelectInput from "../forms/SelectInput";
import { sizes } from "../../config";
import { icons } from "../Icon";

class CmsButton extends React.Component {
  state = {};

  handleSubmit = () => {
    this.props.onUpdate(this.state);
  };

  setParentState = (valueKey, value) => {
    this.setState({ [valueKey]: value }, () => this.handleSubmit());
  };

  render() {
    const {
      data: {
        text,
        size,
        colour,
        onClick,
        disabled,
        classes,
        fullWidth,
        icon,
        type,
        muted,
        id,
        border,
        url,
        isLink,
        download,
        isOutline,
        children
      },
      RouterLink,
      isEditing
    } = this.props;

    if (isEditing) {
      return (
        <div className="">
          <div>
            <Input
              label={`Button Text`}
              setState={this.setParentState}
              valueKey={`text`}
              value={text}
            />
          </div>

          <div className="mt-4">
            <Input
              valueKey={`url`}
              value={url}
              label={`Button URL`}
              setState={this.setParentState}
              description={`Being the URL with a forward slash to create a link to another page on the site, or
                begin the url with http:// for external links.`}
            />
          </div>

          <div className="mt-4">
            <Input
              valueKey={`classes`}
              value={classes}
              label={`Additional CSS Classes`}
              setState={this.setParentState}
            />
          </div>

          <div className="mt-4">
            <Input
              type={`checkbox`}
              setState={this.setParentState}
              label={`Is full width?`}
              value={fullWidth}
              valueKey={`fullWidth`}
            />
          </div>

          <div className="mt-4">
            <Input
              type={`checkbox`}
              setState={this.setParentState}
              label={`Is outline?`}
              value={isOutline}
              valueKey={`isOutline`}
            />
          </div>

          <div className="mt-4">
            <SelectInput
              placeholder={`Select a Size`}
              options={sizes}
              valueKey={`size`}
              value={size}
              label={`Button Size`}
              setState={this.setParentState}
              description={`Being the URL with a forward slash to create a link to another page on the site, or
                begin the url with http:// for external links.`}
            />
          </div>

          <div className="mt-4">
            <SelectInput
              placeholder={`Select an Icon`}
              options={Object.keys(icons)}
              valueKey={`icon`}
              value={icon}
              label={`Icon`}
              setState={this.setParentState}
              description={`Select either an icon, text or both.`}
            />
          </div>

          <div className="mt-4">
            <Input
              fullWidth={false}
              type={`colour`}
              setState={this.setParentState}
              label={`Select a Colour`}
              value={colour}
              valueKey={`colour`}
            />
          </div>
          {/* <button onClick={this.handleSubmit} className="btn w-full" type="submit"> */}
          {/*     Update */}
          {/* </button> */}
        </div>
      );
    }

    return (
      <Button
        text={text}
        size={size}
        colour={colour}
        onClick={onClick}
        disabled={disabled}
        classes={classes}
        fullWidth={fullWidth}
        isOutline={isOutline}
        RouterLink={RouterLink}
        icon={icon}
        type={type}
        muted={muted}
        id={id}
        border={border}
        url={url}
        isLink={isLink}
        download={download}>
        {children}
      </Button>
    );
  }
}

CmsButton.propTypes = {
  data: PropTypes.object.isRequired,
  isEditing: PropTypes.bool.isRequired,
  onUpdate: PropTypes.func
};

CmsButton.defaultProps = {
  isEditing: false
};

export default CmsButton;
