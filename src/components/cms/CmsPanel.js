import React, { Component } from "react";
import PropTypes from "prop-types";
import Input from "../forms/Input";
import Panel from "../forms/Panel";
import SelectInput from "../forms/SelectInput";
import { icons } from "../Icon";
import CmsMarkdown from "./CmsMarkdown";
import Label from "../forms/Label";

class CmsPanel extends Component {
  state = {
    key: Math.random()
      .toString(36)
      .substring(7)
  };

  handleSubmit = () => {
    // console.log("Panel update", this.state);
    this.props.onUpdate(this.state);
  };

  setParentState = (valueKey, value) => {
    this.setState({ [valueKey]: value }, () => this.handleSubmit());
  };

  render() {
    const iconKeys = Object.keys(icons);
    const empty = { value: "", title: "None" };
    iconKeys.unshift(empty);

    const {
      data: {
        title,
        colour,
        overflow,
        background,
        icon,
        containerClasses,
        body
      },
      isEditing
    } = this.props;

    if (isEditing) {
      return (
        <div>
          <div>
            <Input
              elementKey={`random_string_1`}
              label={`Title`}
              value={title}
              valueKey={`title`}
              setState={this.setParentState}
            />
          </div>

          <div className="mt-4">
            <Label label={`Body (Markdown)`} />
            <CmsMarkdown
              onUpdate={data => this.setParentState(`body`, data)}
              isEditing={true}
              label={`Body`}
              value={body}
              valueKey={`body`}
              setState={this.setParentState}
              data={body}
            />
          </div>

          <div className="mt-4">
            <Input
              label={`Container CSS Classes`}
              value={containerClasses}
              valueKey={`containerClasses`}
              setState={this.setParentState}
            />
          </div>

          <div className="mt-4">
            <SelectInput
              options={iconKeys}
              valueKey={`icon`}
              value={icon}
              label={`Icon`}
              setState={this.setParentState}
              description={`Select either an icon, text or both.`}
            />
          </div>

          <div className="mt-4">
            <Input
              fullWidth={false}
              type={`colour`}
              setState={this.setParentState}
              label={`Select a Colour`}
              value={colour}
              valueKey={`colour`}
            />
          </div>

          <div className="mt-4">
            <Input
              fullWidth={false}
              type={`colour`}
              setState={this.setParentState}
              label={`Select a BG Colour`}
              value={background}
              valueKey={`background`}
            />
          </div>
        </div>
      );
    }

    return (
      <div className={containerClasses}>
        <Panel
          title={title}
          colour={colour}
          overflow={overflow}
          background={background}
          icon={icon}>
          <CmsMarkdown data={body} />
        </Panel>
      </div>
    );
  }
}

CmsPanel.propTypes = {
  data: PropTypes.object.isRequired,
  isEditing: PropTypes.bool.isRequired,
  onUpdate: PropTypes.func
};

CmsPanel.defaultProps = {
  isEditing: false
};

export default CmsPanel;
