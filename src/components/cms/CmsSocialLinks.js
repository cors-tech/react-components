import React, { Component } from "react";
import PropTypes from "prop-types";
import Input from "../forms/Input";

class CmsSocialLinks extends Component {
  state = {
    text: this.props.data.text || "",
    url: this.props.data.url || "",
    hash_tags: this.props.data.hash_tags || ""
  };

  handleSubmit = () => {
    this.props.onUpdate(this.state);
  };

  initFb = (d, s, id) => {
    let head = d.getElementsByTagName("head")[0];
    let titleToRemove = document.querySelector("meta[property='og:title']");
    if (titleToRemove) {
      titleToRemove.remove();
    }
    let typeToRemove = document.querySelector("meta[property='og:type']");
    if (typeToRemove) {
      typeToRemove.remove();
    }
    let type = d.createElement("meta");
    type.setAttribute("property", "og:type");
    type.content = "article";
    head.appendChild(type);

    let title = d.createElement("meta");
    title.setAttribute("property", "og:title");
    title.content = this.props.title;
    head.appendChild(title);

    let js,
      fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  };

  initTwitter = (d, s, id) => {
    let js;
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    const fjs = d.getElementsByTagName(s)[0];
    fjs.parentNode.insertBefore(js, fjs);
  };

  componentDidMount() {
    this._isMounted = true;

    if (!this.state.isEditing) {
      this.initFb(document, "script", "facebook-jssdk");
      this.initTwitter(document, "script", "twitter-jssdk");
    }
  }

  setParentState = (valueKey, value) => {
    this.setState({ [valueKey]: value }, () => this.handleSubmit());
  };

  render() {
    const { data, isEditing } = this.props;
    const { text, url, hash_tags } = this.state;

    if (isEditing) {
      return (
        <div className="">
          <div className="">
            <Input
              label={`Tweet/Post Text`}
              valueKey={`text`}
              value={text}
              setState={this.setParentState}
            />
          </div>

          <div className="mt-4">
            <Input
              label={`Share URL`}
              valueKey={`url`}
              value={url}
              setState={this.setParentState}
              description={`Add the page url without the domain name and forward slash, e.g. blog/post-title`}
            />
          </div>

          <div className="mt-4">
            <Input
              label={`Hash Tags`}
              valueKey={`hash_tags`}
              value={hash_tags}
              setState={this.setParentState}
              description={`A space-separated list of tags.`}
            />
          </div>
        </div>
      );
    }

    return (
      <div className="social-links">
        <a
          href="https://twitter.com/share"
          className="twitter-share-button"
          data-text={data.text}
          data-url={`${process.env.REACT_APP_FRONTEND_BASE_URL}${data.url}`}
          data-hashtags={data.hash_tags}
          data-show-count="false">
          Tweet
        </a>

        <div id="fb-root" />

        <div
          className="fb-share-button"
          data-href={`${process.env.REACT_APP_FRONTEND_BASE_URL}${data.url}`}
          data-layout="button"
        />
      </div>
    );
  }
}

CmsSocialLinks.propTypes = {
  data: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  isEditing: PropTypes.bool.isRequired,
  onUpdate: PropTypes.func
};
CmsSocialLinks.defaultProps = {
  isEditing: false
};

export default CmsSocialLinks;
