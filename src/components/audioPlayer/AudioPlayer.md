Generic audio player
```jsx
<AudioPlayer 
    src="https://materials.dominochinese.com/dialogs/24141/phpzh98w2.mp3"
/>

<div className="mt-4">
    <AudioPlayer 
        src="https://materials.dominochinese.com/dialogs/24141/phpzh98w2.mp3"
        mini={true}
    />
</div>
```