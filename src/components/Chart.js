import React, { Component } from "react";
import moment from "moment";
import PropTypes from "prop-types";
import { Bar, Line } from "react-chartjs-2";
import LoadingWrapper from "./LoadingWrapper";
import Panel from "./forms/Panel";

class StatsChart extends Component {
  state = {
    isLoading: true,
    error: null,
    data: null
  };

  componentDidMount() {
    this._isMounted = true;
    this.fetchData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.start_date !== this.props.start_date ||
      prevProps.end_date !== this.props.end_date
    ) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { start_date, end_date, url, type, rawData, get } = this.props;

    if (rawData) {
      return this.safeSetState({
        isLoading: false,
        data: this.parsePlanData(rawData)
      });
    }

    this.safeSetState({
      isLoading: true,
      error: null
    });

    get(
      `/${url}?start_date=` +
        moment(start_date).toJSON() +
        "&end_date=" +
        moment(end_date).toJSON()
    )
      .then(({ data }) => {
        this.safeSetState({
          data:
            type === "stackedBar"
              ? this.parsePlanData(data)
              : this.generateChartData(data.data, data.labels),
          isLoading: false
        });
      })
      .catch(error => {
        this.safeSetState({
          isLoading: false,
          error
        });
      });
  };

  safeSetState = (...args) => {
    this._isMounted && this.setState(...args);
  };

  parsePlanData = ({ labels, gateways }) => {
    let datasets = [];
    const colors = [
      "56, 193, 114",
      "227, 52, 47",
      "52, 144, 220",
      "255, 209, 126",
      "197, 114, 239",
      "56, 193, 114",
      "227, 52, 47",
      "52, 144, 220",
      "255, 209, 126",
      "197, 114, 239"
    ];

    Object.keys(gateways).forEach((gate, index) => {
      datasets.push({
        label: gate,
        data: gateways[gate],
        fill: true,
        lineTension: 0,
        backgroundColor: `rgb(${colors[index]}, .6)`
      });
    });

    return { labels: labels, datasets };
  };

  generateChartData = (data, labels) => {
    return {
      labels: labels,
      datasets: [
        {
          label: this.props.title,
          data: data,
          fill: true,
          pointBackgroundColor: "rgb(52, 144, 220, .3)",
          borderColor: "rgb(52, 144, 220, .3)",
          backgroundColor: "rgb(52, 144, 220, .8)"
        }
      ]
    };
  };

  render() {
    const { isLoading, data, error } = this.state;
    const { type, title, classes } = this.props;

    let chart = <Panel colour={`red`} title={`Invalid chart type`} />;

    if (type === "bar") {
      chart = (
        <div className={classes}>
          <Bar
            data={data}
            options={{
              responsive: true,
              maintainAspectRatio: true,
              title: {
                display: true,
                text: title
              },
              legend: {
                display: false
              },
              scales: {
                yAxes: [
                  {
                    ticks: {
                      beginAtZero: true
                    }
                  }
                ]
              }
            }}
          />
        </div>
      );
    }

    if (type === "stackedBar") {
      chart = (
        <div className={classes}>
          <Bar
            data={data}
            options={{
              responsive: true,
              maintainAspectRatio: true,
              title: {
                display: true,
                text: title
              },
              tooltips: {
                  mode: 'label',
                  callbacks: {
                      label: function(tooltipItem, data) {
                          const corporation = data.datasets[tooltipItem.datasetIndex].label;
                          const value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                          // Loop through all datasets to get the actual total of the index
                          let total = 0;
                          for (let i = 0; i < data.datasets.length; i++)
                              total += data.datasets[i].data[tooltipItem.index];

                          // If it is not the last dataset, you display it as you usually do
                          if (tooltipItem.datasetIndex != data.datasets.length - 1) {
                              return corporation + ": " + value;
                          } else { // .. else, you display the dataset and the total, using an array
                              return [corporation + ": " + value, "Total: " + total];
                          }
                      }
                  }
              },
              legend: {
                display: false
              },
              scales: {
                xAxes: [
                  {
                    ticks: {
                      autoSkip: true
                    },
                    stacked: true
                  }
                ],
                yAxes: [
                  {
                    ticks: {
                      beginAtZero: true
                    },
                    stacked: true
                  }
                ]
              }
            }}
          />
        </div>
      );
    }

    if (type === "line") {
      chart = (
        <div className={classes}>
          <Line
            data={data}
            options={{
              responsive: true,
              maintainAspectRatio: true,
              title: {
                display: true,
                text: title
              },
              legend: {
                display: false
              },
              scales: {
                yAxes: [
                  {
                    ticks: {
                      beginAtZero: false,
                      precision: 0
                    }
                  }
                ]
              }
            }}
          />
        </div>
      );
    }

    return (
      <div className="p-4 shadow border rounded w-full mb-4">
        <LoadingWrapper
          error={error}
          isLoading={isLoading}
          retry={this.fetchData}>
          {data ? chart : null}
        </LoadingWrapper>
      </div>
    );
  }
}

StatsChart.propTypes = {
  start_date: PropTypes.string,
  end_date: PropTypes.string,
  url: PropTypes.string,
  type: PropTypes.string,
  classes: PropTypes.string,
  title: PropTypes.string.isRequired,
  get: PropTypes.func.isRequired,
  rawData: PropTypes.object
};

export default StatsChart;
