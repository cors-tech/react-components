# React Components

## Installation
1. Install tailwindcss

## Customisation
The package already comes with sensible styling defaults, but to customise them, create an environment variable pointing to a js config file of your choice. This will be merged with the default styles.

#### Default config
```js'
{
  styles: {
    inputClasses: `w-full block mt-1 border rounded p-2 text-gray-700`,
    labelClasses: `block text-xs uppercase text-gray-500`,
    panelClasses: `border-l-4 shadow rounded-r p-4 md:p-6`,
    buttonClasses: `bg-white border rounded`
  }
}
```

#### Example config file
```js
module.exports = {
  styles: {}
};
```

#### .env
```
REACT_APP_CUSTOM_STYLE_FILE="/full/path/to/customStyles.js"
```